
-- SUMMARY --

Better 404 is a module based on an article from A List Apart that aims to enhance the usability of 404's error pages.


-- REQUIRENENTS --

Nothing is required for that module.

-- INSTALLATION --

1. Extract better404 on your module directory (ex. sites/all/modules)
2. Go to admin/build/modules and enable Better 404
3. Configure the module in admin/setting/better404

-- CONFIGURATION --

Avbailable on admin/settings/better404

-- TROUBLESHOOTING --

-- FAQ --

-- CONTACT --

Current maintainers:
* Yvan Marques (yvmarques) - http://drupal.org/user/298685
